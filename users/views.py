from django.http.request import HttpRequest
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required

def register(request: HttpRequest) -> HttpResponse:
    if request.method == 'POST':
        # get the information posted in the form
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            # save the new user in the database
            form.save()
            # get the username and prepares a success message to the user
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login')

    else:
        # get a blank form to render in the template
        form = UserRegisterForm()
    return render(request=request, template_name='users/register.html', context={'form': form})

@login_required
def profile(request: HttpRequest) -> HttpResponse:
    #Forms to update both User and Profile models with current User and Profile data
    if request.method == 'POST':
        #Pass the POST data
        user_form = UserUpdateForm(request.POST, instance=request.user)
        #Pass the image file
        profile_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            # save both user and profile in the database
            user_form.save()
            profile_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile')
    else:
        user_form = UserUpdateForm(instance=request.user)
        profile_form = ProfileUpdateForm(instance=request.user.profile)
    context = {
        'user_form': user_form,
        'profile_form': profile_form
    }

    return render(request=request, template_name='users/profile.html', context=context)