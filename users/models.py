from django.db import models
from django.contrib.auth.models import User
from PIL import Image
import os
# Function to rename image name to the same as username
def pic_path(instance, filename):
    _, file_extension = os.path.splitext(filename)
    username = instance.user.username
    return f'profile_pics/{username}{file_extension}'

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to=pic_path)

    def __str__(self) -> str:
        return f'{self.user.username} Profile'

    def save(self):
        # remove old image from the back-end
        try:
            old_img_name = Profile.objects.get(id=self.id).image.name
            old_img_path = Profile.objects.get(id=self.id).image.path
            #This method is called two times, the last after updating the objects
            if self.image.path != old_img_path and old_img_name != 'default.jpg':
                os.remove(old_img_path)
        except:
            pass
        # save method of upper class
        super().save()
        img = Image.open(self.image.path)
        #Resizes image to 300x300 and saves it
        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
