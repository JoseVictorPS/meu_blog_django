from typing import Optional
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from .models import Post
from django.contrib.auth.models import User
from django.views.generic import (
    ListView, DetailView, CreateView, UpdateView, DeleteView
)
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

class PostListView(ListView):
    model = Post
    # <app>/<model>_<viewtype>.html
    template_name = 'blog/home.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 2

class UserPostListView(ListView):
    model = Post
    # <app>/<model>_<viewtype>.html
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 2
    # Filter the objects to display in the template
    def get_queryset(self) -> QuerySet:
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Post.objects.filter(author=user).order_by('-date_posted')
        

class PostDetailView(DetailView):
    model = Post

# has to inherit the Mixin classes before anything else
class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form) -> HttpResponse:
        form.instance.author = self.request.user
        return super().form_valid(form)

# has to inherit the Mixin classes before anything else
class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin,UpdateView):
    model = Post
    fields = ['title', 'content']

    def form_valid(self, form) -> HttpResponse:
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self) -> Optional[bool]:
        post = self.get_object()
        if self.request.user == post.author:
            return True
        else:
            return False

# has to inherit the Mixin classes before anything else
class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    success_url = '/blog'

    def test_func(self) -> Optional[bool]:
        post = self.get_object()
        if self.request.user == post.author:
            return True
        else:
            return False

# Old home function view
'''
def home(request: HttpRequest) -> HttpResponse:
    context = {
        'posts':Post.objects.all()
    }
    return render(request=request, template_name='blog/home.html', context=context)
'''

def about(request: HttpRequest) -> HttpResponse:
    return render(request=request, template_name='blog/about.html', context={'title': 'About'})